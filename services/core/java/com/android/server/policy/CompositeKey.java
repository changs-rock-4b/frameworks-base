/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.server.policy;

import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.android.internal.util.IState;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;

public class CompositeKey {
    private static final boolean DEBUG = true;
    private static final String TAG = "CompositeKey";

    private Context mContext;
    private CompositeKeyStateMachine mCompositeKeyStateMachine;

    private State mNoneState;
    private State mCtrlState;
    private State mAltState;

    CompositeKey(Context context){
        mContext = context;
        mCompositeKeyStateMachine = new CompositeKeyStateMachine("ctrl");
        mNoneState = new NoneState();
        mCtrlState = new CtrlState();
        mAltState  = new AltState();
        mCompositeKeyStateMachine.addState(mNoneState);
        mCompositeKeyStateMachine.addState(mCtrlState);
        mCompositeKeyStateMachine.addState(mAltState);
        mCompositeKeyStateMachine.setInitialState(mNoneState);
        mCompositeKeyStateMachine.start();
    }

    public boolean sendKeyEvent(KeyEvent event){
        if(DEBUG) Log.d(TAG, "sendKeyEvent: " + event.getKeyCode());

            IState state = mCompositeKeyStateMachine.getCurrentState();
            if(state != null && state == mAltState){
                Message message = new Message();
                message.obj = event;
                mCompositeKeyStateMachine.sendMessage(message);
                return ((AltState)mAltState).interceptKey(event);
            }else{
                if(event.getAction() == KeyEvent.ACTION_DOWN && event.getRepeatCount() == 0){
                    mCompositeKeyStateMachine.sendMessage(event.getKeyCode());
                }
            }

        return false;
    }

    private class CompositeKeyStateMachine extends StateMachine{
        CompositeKeyStateMachine(String name) {
            super(name);
        }
    }

    private class NoneState extends State{

        @Override
        public boolean processMessage(Message msg) {
            switch (msg.what){
                case KeyEvent.KEYCODE_CTRL_LEFT:
                case KeyEvent.KEYCODE_CTRL_RIGHT:
                    if(DEBUG) Log.d(TAG, "NoneState transitionTo CtrlState");
                    mCompositeKeyStateMachine.transitionTo(mCtrlState);
                    break;
                default:
                    mCompositeKeyStateMachine.transitionTo(mNoneState);
                    break;

            }
            return true;
        }
    }

    private class CtrlState extends State{
        @Override
        public boolean processMessage(Message msg) {
            switch (msg.what){
                case KeyEvent.KEYCODE_ALT_LEFT:
                case KeyEvent.KEYCODE_ALT_RIGHT:
                    if(DEBUG) Log.d(TAG, "CtrlState transitionTo AltState");
                    mCompositeKeyStateMachine.transitionTo(mAltState);
                    break;
                default:
                    mCompositeKeyStateMachine.transitionTo(mNoneState);
                    break;
            }
            return true;
        }
    }

    private class AltState extends State{

        private boolean doActionDown(int keycode){
            switch (keycode){
                case KeyEvent.KEYCODE_P:
                    if(DEBUG) Log.d(TAG, "AltState Power");
                    try {
                        Intent intent = new Intent("com.android.tuner.PowerDialog");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivityAsUser(intent, UserHandle.CURRENT);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    return true;
                case KeyEvent.KEYCODE_ENTER:
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    return true;
                default:
                    break;
            }

            return false;
        }

        private boolean doActionUp(int keycode){
            switch (keycode){
                case KeyEvent.KEYCODE_P:
                    if(DEBUG) Log.d(TAG, "AltState Power To None");
                    mCompositeKeyStateMachine.transitionTo(mNoneState);
                    return true;
                case KeyEvent.KEYCODE_ENTER:
                    if(DEBUG) Log.d(TAG, "AltState Key Enter");
                    SystemProperties.set("persist.sys.tuner.key_enter_dpad", "true");
                    Toast.makeText(mContext, "KeyCode Enter->DpCenter On", Toast.LENGTH_LONG).show();
                    if(DEBUG) Log.d(TAG, "AltState Key Enter To None");
                    mCompositeKeyStateMachine.transitionTo(mNoneState);
                    return true;
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    if(DEBUG) Log.d(TAG, "AltState Key Dpad_Center");
                    SystemProperties.set("persist.sys.tuner.key_enter_dpad",  "false");
                    Toast.makeText(mContext, "KeyCode Enter->DpCenter Off", Toast.LENGTH_LONG).show();
                    if(DEBUG) Log.d(TAG, "AltState Key Dpad_Center To None");
                    mCompositeKeyStateMachine.transitionTo(mNoneState);
                    return true;
                case KeyEvent.KEYCODE_ALT_LEFT:
                case KeyEvent.KEYCODE_ALT_RIGHT:
                    break;
                default:
                    mCompositeKeyStateMachine.transitionTo(mNoneState);
                    break;
            }

            return false;
        }

        private boolean doActionRepeat(int keycode){
            switch (keycode){
                case KeyEvent.KEYCODE_P:
                    return true;
                case KeyEvent.KEYCODE_ENTER:
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    return true;
                default:
                    break;
            }

            return false;
        }

        boolean interceptKey(KeyEvent event){
            return doActionRepeat(event.getKeyCode());
        }

        @Override
        public boolean processMessage(Message msg) {
            KeyEvent event = (KeyEvent) msg.obj;

            if(event.getAction() == KeyEvent.ACTION_DOWN){
                if(event.getRepeatCount() == 0){
                    return doActionDown(event.getKeyCode());
                }else {
                    return doActionRepeat(event.getKeyCode());
                }
            }else{
                return doActionUp(event.getKeyCode());
            }
        }
    }
}
