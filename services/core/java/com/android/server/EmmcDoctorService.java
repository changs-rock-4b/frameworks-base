
package com.android.server;
import android.content.Context;
import android.os.IEmmcDoctorService;
import android.util.Slog;
public class EmmcDoctorService extends IEmmcDoctorService.Stub {
    private static final String TAG = "EmmcDoctorService";
    EmmcDoctorService() {
        boolean ret = init_native();
        if (!ret) {
            Slog.v("EmmcDoctorService", "init_native succeeded");
        } else {
            Slog.v("EmmcDoctorService", "init_native failed");
        }
    }
    public int getSlcLife() {
        Slog.v("EmmcDoctorService", "getSlcLife");
        return getSlcLife_native();
    }
    public int getMlcLife() {
        Slog.v("EmmcDoctorService", "getMlcLife");
        return getMlcLife_native();
    }

    private static native boolean init_native();
    private static native int getSlcLife_native();
    private static native int getMlcLife_native();
};
