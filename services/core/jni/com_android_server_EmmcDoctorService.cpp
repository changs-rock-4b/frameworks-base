
#define LOG_TAG "EMMC_DOC_SERVICE_JNI"
#include "jni.h"
#include <nativehelper/JNIHelp.h>
#include "android_runtime/AndroidRuntime.h"
#include <utils/misc.h>
#include <utils/Log.h>
#include <hardware/hardware.h>
#include <hardware/emmc_doc.h>
#include <stdio.h>


namespace android
{
    // HAL component defined in <hardware/emmc_doc.h>
    struct emmc_doc_device_t* device = NULL;
    struct emmc_doc_module_t* module = NULL;

    // Open device through HAL Module
    static inline int open_device(const hw_module_t* module, struct emmc_doc_device_t** device) {
        // 2nd Param could be used to open different types of hw_device_t
        return module->methods->open(module, EMMC_HEALTH_HARDWARE_MODULE_ID, (struct hw_device_t**)device);
    }

    // Read EMMC life time registers through HAL
    static jint get_life(JNIEnv* env, jobject clazz, int is_slc) {
        int slc, mlc;
        ALOGI("get_life() started, &module == %p", module);
        if (open_device((struct hw_module_t*)module, &device) != 0) {
            ALOGE("Failed to open device");
            return -1;
        }
        ALOGI("open_device() succeeded &device == %p", device);
        device->get_life_time(device, &mlc, &slc);
        device->common.close((struct hw_device_t*)device);
        return is_slc? slc: mlc;
    }

    // Read EMMC life time registers through HAL
    static jint get_slc_life(JNIEnv* env, jobject clazz) {
        return get_life(env, clazz, 1);
    }

    // Read EMMC life time registers through HAL
    static jint get_mlc_life(JNIEnv* env, jobject clazz) {
        return get_life(env, clazz, 0);
    }

    // Load HAL.so file to load the HAL Module
    static jboolean init_module(JNIEnv* env, jclass clazz) {
        ALOGI("initModule()");
        if(hw_get_module(EMMC_HEALTH_HARDWARE_MODULE_ID, (const struct hw_module_t**)&module) == 0) {
            ALOGI("initModule() Module found");
            return 0;
//            if(hello_device_open(&(module->common), &hello_device) == 0) {
//                ALOGI("Hello JNI: hello device is open.");
//                return 0;
//            }
//            ALOGE("Hello JNI: failed to open hello device.");
//            return -1;
        }
        ALOGE("initModule() failed, can't find HAL module");
        return -1;		
    }
    // JNI methods table
    static const JNINativeMethod method_table[] = {
        {"init_native", "()Z", (void*)init_module},
        {"getSlcLife_native", "()I", (void*)get_slc_life},
        {"getMlcLife_native", "()I", (void*)get_mlc_life},
    };
    /*************************************************************
     * MAIN ENTRANCE
     * Register JNI methods table, main entrance
     * Called in onload.cpp
     *************************************************************
     */
    int register_android_server_EmmcDoctorService(JNIEnv *env) {
        return jniRegisterNativeMethods(env, "com/android/server/EmmcDoctorService", method_table, NELEM(method_table));
    }
};

