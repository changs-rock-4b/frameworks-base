package android.os;

import android.annotation.SystemService;
import android.annotation.SystemService;
import android.content.Context;

@SystemService("EmmcDoctor")
public class EmmcDoctor {
    private final IEmmcDoctorService service;
    public EmmcDoctor(Context context, IEmmcDoctorService service) {
        this.service = service;
    }

    public int getSlcLife() {
        try {
            return service.getSlcLife();
        } catch (RemoteException e) {
            throw e.rethrowFromSystemServer();
        }
    }

    public int getMlcLife() {
        try {
            return service.getMlcLife();
        } catch (RemoteException e) {
            throw e.rethrowFromSystemServer();
        }
    }
}
